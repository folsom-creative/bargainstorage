<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bargainstorage
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<div class="mobile-word-wrap">
		<img class="word-logo" src="<?php bloginfo('template_url'); ?>/img/mobile_wordmark.png"/>
	</div>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bargainstorage' ); ?></a>

	<header id="masthead" class="site-header" role="banner">


		<div class="top-wrapper">
			<div class="left">
				<div class="content">
					<?php if(is_page('home') || is_page('storage-facilities')) { ?>
						<a href="#find-a-storage-unit" class="scrollto">
							<img src="<?php bloginfo('template_url'); ?>/img/location.png">
							<h5>FIND LOCATION</h5>
						</a>
					<?php } else { ?>
						<a href="<?php echo get_permalink(73); ?>">
							<img src="<?php bloginfo('template_url'); ?>/img/location.png">
							<h5>FIND LOCATION</h5>
						</a>
					<?php } ?>
				</div>
			</div>

			<div class="middle vertical-parent">
				<div class="vertical-center">
					<a class="logo" href="<?php echo get_option('home'); ?>">
						<img src="<?php bloginfo('template_url'); ?>/img/logo.png">
					</a>
				</div>
			</div>

			<div class="right">
				<div class="content">
					<div class="social">
						<a href="http://facebook.com" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/bargainstorage" target="_blank"><i class="fa fa-twitter"></i></a>
						<a href="http://linkedin.com" target="_blank"><i class="fa fa-linkedin"></i></a>
					</div>
					<a href="<?php echo get_permalink(523); ?>"><h5>LOGIN REGISTER</h5></a>
				</div>
			</div>
		</div>
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav>
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'portal', 'menu_id' => 'portal-menu' ) ); ?>
		</nav>


	</header><!-- #masthead -->

	<header id="mobile-masthead" class="mobile-header" role="banner">

		<div class="mobile-blue-bar">

			<a id="nav-trigger" href="#mobile-navigation"><i class="fa fa-bars"></i></a>
			<div id="mobile-logo-wrap">
				<a class="home-link" href="<?php echo get_option('home'); ?>">
					<div class="band-aid"></div>
					<img class="m-logo" src="<?php bloginfo('template_url'); ?>/img/mobile_mark.png">
				</a>
			</div>

		</div>


	</header>

	<div id="content" class="site-content">
