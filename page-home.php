<?php
/**
 * Template Name: Landing page
 *
 * @package bargainstorage
 */

get_header(); ?>


<?php
// This snippet gets the url of the current page's featured image
$imgURL = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
?>
<div id="gif-hero">
    <img src="<?php echo $imgURL; ?>"/>
</div>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="entry-content">
            <section class="grid-bg">
                <div class="scallop-top"></div>
                <h1 class="section-heading"><?php the_field('main_heading'); ?></h1>

                <div class="section-tagline">
                    <p>
                        <?php the_field_without_wpautop('top_content'); ?>
                    </p>
                </div>

                <div class="holder">
                    <div class="half">
                        <h4><?php the_field('top_left_heading'); ?></h4>
                        <p>
                            <?php the_field_without_wpautop('top_left_content'); ?>
                        </p>
                    </div>

                    <div class="half">
                        <h4><?php the_field('top_right_heading'); ?></h4>
                        <p>
                            <?php the_field_without_wpautop('top_right_content'); ?>
                        </p>
                    </div>
                </div>

                <a href="<?php echo get_permalink(69); ?>" class="hex-button">
                    <img src="<?php bloginfo('template_url'); ?>/img/buttons/learn-more.png">
                </a>
                <div class="scallop-bottom"></div>
            </section> <!-- .wrapper -->
            <section class="banner-image" style="background-image: url(<?php bloginfo('template_url'); ?>/img/home-hero-2.jpg);">

            </section>
            <section id="find-a-storage-unit" class="grid-bg unpad">
                <div class="scallop-top"></div>
                <h3>FIND A STORAGE UNIT <br> OR PARKING SPACE NEAR YOU.</h3>
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; // End of the loop. ?>
                <div class="scallop-bottom"></div>
            </section> <!-- .wrapper -->
            <section class="banner-image" style="background-image: url(<?php bloginfo('template_url'); ?>/img/home-hero-3.jpg);background-position-y: 6%;">

            </section>
            <section class="grid-bg pad">
                <div class="scallop-top"></div>
                <h1 class="section-heading">Reviews</h1>
                <div id="reviews" class="review-wrap" data-columns>

                    <!-- check if the repeater field has rows of data -->
                    <?php if( have_rows('reviews') ): ?>

                        <?php while ( have_rows('reviews') ) : the_row(); ?>

                            <div class="review">
                                <span class="stars">
                                    <?php
                                        $select = get_sub_field_object('stars');
                                        $value = get_sub_field('stars');
                                    ?>
                                    <?php foreach( $select['choices'] as $k => $v ): ?>
                                        <?php if( $k == $value ): ?>
                                            <?php
                                            if ($value == 'four') {
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                            else if ($value == 'five') {
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                            ?>

                                        <?php endif; ?>

                                    <?php endforeach; ?>


                                </span>
                                <p> <?php the_sub_field('review'); ?> </p>
                                <span class="source"> <?php  the_sub_field('source'); ?> </span>
                            </div>

                    <?php endwhile; ?>

                    <?php endif; ?>

                </div>
                <div id="mobile-reviews" class="review-wrap">
                    <!-- Mobile touch slider -->
                    <?php if( have_rows('reviews') ): ?>

                    <div class="owl-carousel">
                        <?php while ( have_rows('reviews') ) : the_row(); ?>
                            <div class="review">
                                <span class="stars">
                                    <?php
                                        $select = get_sub_field_object('stars');
                                        $value = get_sub_field('stars');
                                    ?>
                                    <?php foreach( $select['choices'] as $k => $v ): ?>
                                        <?php if( $k == $value ): ?>
                                            <?php
                                            if ($value == 'four') {
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                            else if ($value == 'five') {
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                                echo '<i class="fa fa-star"></i>';
                                            }
                                            ?>

                                        <?php endif; ?>

                                    <?php endforeach; ?>


                                </span>
                                <p> <?php the_sub_field('review'); ?> </p>
                                <span class="source"> <?php  the_sub_field('source'); ?> </span>
                            </div>

                    <?php endwhile; ?>
                        </div>
                    <?php endif; ?>

                    <span href="" class="hex-button">
                        <img src="<?php bloginfo('template_url'); ?>/img/buttons/swipe.png">
                    </span>
                </div>
            </section>

        </div> <!-- .entry-content -->
    </main><!-- #main -->
</div><!-- #primary -->

<script>
    jQuery(document).ready(function($){
        /* Owl Carousel (Reviews slider)
        --------------------------------*/
        var owl = $('.owl-carousel');

        owl.owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            items: 1,
            center: true,
            draggable: true
        });

        // Go to the next item
        $('.custom-next').click(function() {
            owl.trigger('next.owl.carousel');
        })

        // Go to the previous item
        $('.custom-prev').click(function() {
            // With optional speed parameter
            // Parameters have to be in square brackets '[]'
            owl.trigger('prev.owl.carousel', [300]);
        })

        /* Hammer.js swipe library
        --------------------------*/
        // var hammertime = new Hammer(myElement, myOptions);

        // hammertime.on('swipeleft', function(ev) {
        //     console.log("hammer swiped left");
        // });

        // hammertime.on('swiperight', function(ev) {
        //     console.log("hammer swiped right");
        // });

    });
</script>
<?php get_footer(); ?>
