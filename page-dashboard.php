<?php
/**
 * Template Name: Dashboard Home
 *
 * @package bargainstorage
 */
 if ( is_user_logged_in() ) {
 } else {
     wp_redirect('http://bargainstorage.bldsvr.com/bargainstoragev2/login/'); exit;
 }
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="entry-content">
            <section>
                <h1 class="section-heading"><?php the_field('main_heading'); ?></h1>
                <p class="section-tagline" style="margin-bottom: 2em;">
					<?php the_field_without_wpautop('top_content'); ?>
                </p>
                <img class="" src="<?php bloginfo('template_url'); ?>/img/BS_Portal.gif" />
            </section>

            <section class="black pad">
				<div class="scallop-top black"></div>

                <h1 class="section-heading white"><?php the_field('white_heading'); ?></h1>
                <img class="" src="<?php bloginfo('template_url'); ?>/img/number-thing.png" />

				<div class="scallop-bottom black"></div>
            </section>

            <section class="banner-image" style="background-image: url(<?php bloginfo('template_url'); ?>/img/portal-hero.jpg);">

            </section>

            <section id="meet-the-team" class="pos-rel">
                <div class="scallop-top white-border"></div>
                <h1 class="section-heading"><?php the_field('teammates_heading'); ?></h1>
                <p class="section-tagline">
                    <?php the_field_without_wpautop('teammates_content'); ?>
                </p>
                <div class="holder">
                    <?php
					    $loop = new WP_Query(
						    array(
							'post_type' => 'teammate',
                            'category_name' => 'portal',
                            'order' => 'ASC'
						)
					);
				    ?>

                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <?php
                            // get url of current post's featured image
                            $imgPath = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
                        ?>

                        <div class="third">
                            <div class="team-pic" class style="background-image: url('<?php echo $imgPath; ?>');">

                            </div>
                            <div class="team-pic-fix">

                                <span class="name-underline"> <?php the_title(); ?> </span>
                                <div class="employee-description">
                                    <span> <?php the_field('job_title'); ?></span> <br>
                                </div>

                            </div>

                        </div>

                    <?php endwhile; wp_reset_query(); // reset query at end of the loop. ?>

                </div>
            </section>

            <section id="our-history" class="grid-bg" style="padding: 1em 0 2em;">
				<div class="scallop-top"></div>
                <h1 class="section-heading"><?php the_field('our_history_heading'); ?></h1>
                <p class="section-tagline">
                    <?php the_field_without_wpautop('our_history_content'); ?>
                </p>
                <img class="" src="<?php bloginfo('template_url'); ?>/img/our-history.png" />

				<div class="scallop-bottom"></div>
            </section>

            <section class="get-in-touch-wrap ">
                <h1 class="section-heading"><?php the_field('contact_section_heading'); ?></h1>
                <p class="section-tagline">
                    <?php the_field_without_wpautop('contact_section_content'); ?>
                </p>
				<div class="application-form">
                    <?php echo do_shortcode('[contact-form-7 id="664" title="Get In Touch"]'); ?>
                </div>
            </section>

        </div> <!-- .entry-content -->
    </main><!-- #main -->
</div><!-- #primary -->

<script type="text/javascript">
    jQuery(document).ready(function($){
        function imgHeight() {
            $('.team-pic').each(function(){
                picWidth = $(this).width();
                $(this).height(picWidth);
            });
        }

        $(window).resize(function() {
            imgHeight();
        }).resize();
    });
</script>

<?php get_footer(); ?>
