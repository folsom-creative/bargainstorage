<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bargainstorage
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div id="secondary" class="widget-area one-third" role="complementary">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
	<a href="<?php echo get_permalink(73); ?>">
		<img class="usa-map" src="<?php bloginfo('template_url'); ?>/img/usa.png">
	</a>
</div><!-- #secondary -->
