<?php
/**
 * Template Name: Gallery
 *
 * @package bargainstorage
 */
 if ( is_user_logged_in() ) {
 } else {
 	wp_redirect('http://bargainstorage.bldsvr.com/bargainstoragev2/login/'); exit;
 }
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="entry-content">
            <section class="pad">
                <?php while ( have_posts() ) : the_post(); ?>

                    <h1 class="section-heading"><?php the_title(); ?></h1>
                    <div class="section-tagline">
    					<?php the_content(); ?>
                    </div>

                <?php endwhile; // End of the loop. ?>
                
                <div id="filters" class="filter">
                    <div class="menu">
                        <button data-filter="*" id="all" class="button">All</button>
                        <button data-filter=".before, .after" id="before" class="button">Before/After</button>
                        <!-- <button data-filter=".after" id="after" class="button">After</button> -->
                        <button data-filter=".colorado" id="colorado" class="button">Colorado</button>
                        <button data-filter=".arizona" id="arizona" class="button">Arizona</button>
                        <button data-filter=".nevada" id="nevada" class="button">Nevada</button>
                        <button data-filter=".texas" id="texas" class="button">Texas</button>
                    </div>
                </div>

                <?php 
                    if( have_rows('gallery_images') ):
                        echo '<div id="gallery">';
                        echo '<div class="gutter-sizer"></div>';
                        echo '<div class="grid-sizer"></div>';
                     
                        while ( have_rows('gallery_images') ) : the_row();
                            $tags = get_sub_field('image_tag'); 
                ?>
                            <a class="gallery-item <?php echo join(' ', $tags); ?>" style="background-image: url('<?php the_sub_field('image_file'); ?>')" href="<?php the_sub_field('image_file'); ?>">
                                <div class="tag-bg">
                                    <span class="img-tag"><?php echo join(', ', $tags); ?></span>
                                </div>
                            </a>
                <?php 
                        endwhile;
                        echo '</div>';
                    else :

                        // no rows found

                    endif;
                ?>

            </section>
        </div> <!-- .entry-content -->
    </main><!-- #main -->
</div><!-- #primary -->

<script type="text/javascript">
    jQuery(document).ready(function($){

        /* Isotope Gallery
        ------------------*/
        var $isoContainer = $('#gallery');
        var filterValue = '*';

        $isoContainer.imagesLoaded(function(){
            $isoContainer.isotope({
                filter: filterValue,
                itemSelector: '.gallery-item'
            });

        /* Magnific Lightbox Gallery
        ----------------------------*/
            $('.gallery-item').magnificPopup({
                type: 'image',
                gallery:{
                    enabled: true
                }
            });
        });

        // apply filters
        $('#filters').on( 'click', 'button', function() {

            filterValue = $(this).attr('data-filter');

            $isoContainer.isotope({
                filter: filterValue,
                itemSelector: '.gallery-item'
            });
        });


        $('.button').click(function(){
            $('.button').removeClass('active');
            $(this).addClass('active');

        });
    });
</script>

<?php get_footer(); ?>
