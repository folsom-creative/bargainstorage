<?php
/**
 * Template Name: Meet The Team
 *
 * @package barginstorage
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="entry-content">
            <section>
                <h1 class="section-heading">Our Proven Formula</h1>
                <p class="section-tagline">
                    We take neglected, broken-down storage facilities, fix them up and make them run better. (Okay, it’s a little more complicated than that.)
                </p>

                <a href="<?php echo get_permalink(); ?>" class="hex-button">
                    <img src="<?php bloginfo('template_url'); ?>/img/buttons/learn-more.png">
                </a>

            </section>

            <section class="black-bg" >
                <h1 class="section-heading white">Statistics</h1>

            </section>

            <section>
                <h1 class="section-heading">What Sets Us Apart</h1>
                <p class="section-tagline">
                    We believe that if you invest in a strong team (hire right, create a strong culture and train well), a small group can produce out-sized results. Here’s just a little background our great team and what we’ve accomplished together.
                </p>
                <div class="holder">
                    <div class="quarter">
                        <div>
                            <img class="team-pic" src="<?php bloginfo('template_url'); ?>/img/raul.png" />
                        </div>
                        <span class="name-underline">Raul</span>
                        <div class="employee-description">
                            <span>Property Manager,</span> <br>
                            <span>East Fort Worth Bargain Storage</span>
                        </div>
                    </div>

                    <div class="quarter">
                        <div>
                            <img class="team-pic" src="<?php bloginfo('template_url'); ?>/img/raul.png" />
                        </div>
                        <span class="name-underline">Raul</span>
                        <div class="employee-description">
                            <span>Property Manager,</span> <br>
                            <span>East Fort Worth Bargain Storage</span>
                        </div>
                    </div>

                    <div class="quarter">
                        <div>
                            <img class="team-pic" src="<?php bloginfo('template_url'); ?>/img/connie.png" />
                        </div>
                        <span class="name-underline">Connie</span>
                        <div class="employee-description">
                            <span>Property Manager,</span> <br>
                            <span>East Fort Worth Bargain Storage</span>
                        </div>
                    </div>

                    <div class="quarter">
                        <div>
                            <img class="team-pic" src="<?php bloginfo('template_url'); ?>/img/jenyfer.png" />
                        </div>
                        <span class="name-underline">Jenyfer</span>

                        <div class="employee-description">

                            <span>Property Manager,</span> <br>
                            <span>East Fort Worth Bargain Storage</span>
                        </div>
                    </div>

                    <div class="quarter">
                        <div>
                            <img class="team-pic" src="<?php bloginfo('template_url'); ?>/img/raul.png" />
                        </div>
                        <span class="name-underline">Raul</span>
                        <div class="employee-description">
                            <span>Property Manager,</span> <br>
                            <span>East Fort Worth Bargain Storage</span>
                        </div>
                    </div>

                    <div class="quarter">
                        <div>
                            <img class="team-pic" src="<?php bloginfo('template_url'); ?>/img/raul.png" />
                        </div>
                        <span class="name-underline">Raul</span>
                        <div class="employee-description">
                            <span>Property Manager,</span> <br>
                            <span>East Fort Worth Bargain Storage</span>
                        </div>
                    </div>

                    <div class="quarter">
                        <div>
                            <img class="team-pic" src="<?php bloginfo('template_url'); ?>/img/connie.png" />
                        </div>
                        <span class="name-underline">Connie</span>
                        <div class="employee-description">
                            <span>Property Manager,</span> <br>
                            <span>East Fort Worth Bargain Storage</span>
                        </div>
                    </div>

                    <div class="quarter">
                        <div>
                            <img class="team-pic" src="<?php bloginfo('template_url'); ?>/img/jenyfer.png" />
                        </div>
                        <span class="name-underline">Jenyfer</span>

                        <div class="employee-description">

                            <span>Property Manager,</span> <br>
                            <span>East Fort Worth Bargain Storage</span>
                        </div>
                    </div>

                </div>
            </section>

            <section class="grid-bg">
                <h1 class="section-heading">Our History</h1>

                <p class="section-tagline">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
            </section>

            <section>
                <h1 class="section-heading">Get In Touch</h1>
                <p class="section-tagline">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <h1 class="section-heading">A contact form similar to the login and register forms</h1>
            </section>

        </div> <!-- .entry-content -->
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
