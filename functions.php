<?php
/**
 * bargainstorage functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package bargainstorage
 */

if ( ! function_exists( 'bargainstorage_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bargainstorage_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on bargainstorage, use a find and replace
	 * to change 'bargainstorage' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'bargainstorage', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'bargainstorage' ),
		'mobile' => esc_html__( 'Mobile Menu', 'bargainstorage' ),
		'portal' => esc_html__( 'Portal Menu', 'bargainstorage' ),
		'portal-mobile' => esc_html__( 'Portal Mobile Menu', 'bargainstorage' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'bargainstorage_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // bargainstorage_setup
add_action( 'after_setup_theme', 'bargainstorage_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bargainstorage_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bargainstorage_content_width', 640 );
}
add_action( 'after_setup_theme', 'bargainstorage_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bargainstorage_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bargainstorage' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'bargainstorage_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function bargainstorage_scripts() {
	wp_enqueue_script( 'fonts', 'http://fast.fonts.net/jsapi/13c758b8-9983-416f-8d46-9958976dc703.js', array(), '20150910');
	wp_enqueue_style( 'bargainstorage-style', get_stylesheet_uri() );
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '20150910' );

	wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/js/owl.carousel.min.css', array(), '20150928' );
	wp_enqueue_style( 'owl-default-theme', get_template_directory_uri() . '/js/owl.theme.default.min.css', array(), '20150928' );
	wp_enqueue_style( 'magnific-css', get_template_directory_uri() . '/js/magnific-popup.css', array(), '20150928' );
	wp_enqueue_style( 'mmenu', get_template_directory_uri() . '/js/jquery.mmenu.css', array(), '20150928' );
	wp_enqueue_style( 'mmenu-fullscreen', get_template_directory_uri() . '/js/jquery.mmenu.fullscreen.css', array(), '20150928' );

	wp_enqueue_script( 'bargainstorage-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'bargainstorage-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script('jquery');
	wp_enqueue_script( 'salvattore', get_template_directory_uri() . '/js/salvattore.min.js', array(), '20150910', true );
	wp_enqueue_script( 'hammer', get_template_directory_uri() . '/js/hammer.min.js', array(), '20150927', true );
	wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), '20150927', true );
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20150927', true );
	wp_enqueue_script( 'mmenu', get_template_directory_uri() . '/js/jquery.mmenu.min.js', array(), '20150928', true );
	wp_enqueue_script( 'main.js', get_template_directory_uri() . '/js/main.js', array(), '20150929', true );

	if ( is_page_template( 'page-storage.php' || 'page-gallery.php' ) ) {
		wp_deregister_script('masonry');
		wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array(), '20150302', true);
		wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array(), '20150301', true);

	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bargainstorage_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';



// Hide admin bar
add_filter('show_admin_bar', '__return_false');

// Register Teammate Post Type
function teammate_post_type() {

	$labels = array(
		'name'                => _x( 'Teammates', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Teammate', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Teammates', 'text_domain' ),
		'name_admin_bar'      => __( 'Teammates', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Episode:', 'text_domain' ),
		'all_items'           => __( 'All Teammates', 'text_domain' ),
		'add_new_item'        => __( 'Add New Teammate', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Teammate', 'text_domain' ),
		'edit_item'           => __( 'Edit Teammate', 'text_domain' ),
		'update_item'         => __( 'Update Teammate', 'text_domain' ),
		'view_item'           => __( 'View Teammate', 'text_domain' ),
		'search_items'        => __( 'Search Teammate', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Teammate', 'text_domain' ),
		'description'         => __( 'Bargain Storage Teammates', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'teammate', $args );

}
add_action( 'init', 'teammate_post_type', 0 );

// Register Teammate Post Type
function press_release_post_type() {

	$labels = array(
		'name'                => _x( 'Press Releases', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Press Release', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Press Releases', 'text_domain' ),
		'name_admin_bar'      => __( 'Press Releases', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Press Release:', 'text_domain' ),
		'all_items'           => __( 'All Press Releases', 'text_domain' ),
		'add_new_item'        => __( 'Add New Press Release', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Press Release', 'text_domain' ),
		'edit_item'           => __( 'Edit Press Release', 'text_domain' ),
		'update_item'         => __( 'Update Press Release', 'text_domain' ),
		'view_item'           => __( 'View Press Release', 'text_domain' ),
		'search_items'        => __( 'Search Press Releases', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'Press Release', 'text_domain' ),
		'description'         => __( 'Bargain Storage Press Releases', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'page-attributes', 'post-formats', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'press_release', $args );

}
add_action( 'init', 'press_release_post_type', 0 );


// Control Excerpt Length using Filters
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Make the "read more" link to the post
function new_excerpt_more( $more ) {
	return '<a class="read-more" href="' . get_permalink(get_the_ID()) . '">' . '<img class="read-more-button" src="' . get_template_directory_uri() . '/img/buttons/read-more.png"/>' . '</a>';
}

add_filter( 'excerpt_more', 'new_excerpt_more' );


// talkshow_posted_on

if ( ! function_exists( 'bargainstorage_posted_on' ) ) :

	function bargainstorage_posted_on() {

	        // Set up and print post meta information.
	        printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date" datetime="%2$s">%3$s</time></a></span> <span class="byline"><span class="author vcard"><a class="url fn n" href="%4$s" rel="author">%5$s</a></span></span>',
	                esc_url( get_permalink() ),
	                esc_attr( get_the_date( 'F j, Y' ) ),
	                esc_html( get_the_date() ),
	                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
	                get_the_author()
	        );
	}
endif;

// add website url to backend
add_filter( 'wpsl_meta_box_fields', 'custom_meta_box_fields' );

function custom_meta_box_fields( $meta_fields ) {

    $meta_fields[__( 'Website', 'wpsl' )] = array(
        'website_url' => array(
            'label' => __( 'Website URL', 'wpsl' )
        )
    );

    return $meta_fields;
}

// add website url to json response
add_filter( 'wpsl_frontend_meta_fields', 'custom_frontend_meta_fields' );

function custom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_website_url'] = array(
        'name' => 'website_url',
        'type' => 'url'
    );

    return $store_fields;
}

/* WPSL Infobox Customization
-----------------------------*/
add_filter( 'wpsl_info_window_template', 'custom_info_window_template' );

function custom_info_window_template() {

    $info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window">' . "\r\n";
    $info_window_template .= "\t\t" . '<p>' . "\r\n";
    $info_window_template .= "\t\t\t" .  wpsl_store_header_template() . "\r\n";
    $info_window_template .= "\t\t\t" . '<span><%= address %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span><%= address2 %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n";
    $info_window_template .= "\t\t" . '</p>' . "\r\n";

    /**
     * Include the data from a custom field called 'website_url'.
     *
     * Before you can access the 'website_url' data in the template,
     * you first need to make sure the data is included in the JSON output.
     *
     * You can make the data accessible through the wpsl_frontend_meta_fields filter.
     */
    $info_window_template .= "\t\t" . '<% if ( website_url ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<a class="directions" href="<%= website_url %>" target="_blank">Website</a>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";

    $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>' . "\r\n";

    return $info_window_template;
}

// change infobox styling on map
add_filter( 'wpsl_infobox_settings', 'custom_infobox_settings' );

function custom_infobox_settings( $infobox_settings ) {

    $infobox_settings['infoBoxClass'] = 'wpsl-infobox';
    $infobox_settings['infoBoxCloseMargin'] = '2px';
    $infobox_settings['infoBoxCloseUrl'] = '//www.google.com/intl/en_us/mapfiles/close.gif';
    $infobox_settings['infoBoxClearance'] = '40,40';
    $infobox_settings['infoBoxDisableAutoPan'] = 0;
    $infobox_settings['infoBoxEnableEventPropagation'] = 0;
    $infobox_settings['infoBoxPixelOffset'] = '-52,-45';
    $infobox_settings['infoBoxZindex'] = 1500;

    return $infobox_settings;
}

/* WPSL Search Results Customization
------------------------------------*/
add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template() {

    global $wpsl_settings;

    $listing_template = '<li data-store-id="<%= id %>">' . "\r\n";
    $listing_template .= "\t\t" . '<div>' . "\r\n";
    $listing_template .= "\t\t\t" . '<p><%= thumb %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . wpsl_store_header_template( 'listing' ) . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address2 %></span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<% } %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n";
    $listing_template .= "\t\t\t\t" . '<span class="wpsl-country"><%= country %></span>' . "\r\n";
    $listing_template .= "\t\t\t" . '</p>' . "\r\n";

    // customizations
    $listing_template .= "\t\t" . '<% if ( website_url ) { %>' . "\r\n";
    $listing_template .= "\t\t" . '<a class="directions" href="<%= website_url %>" target="_blank">Website</a>' . "\r\n";
    $listing_template .= "\t\t" . '<% } %>' . "\r\n";

    // Check if we need to show the distance.
    // if ( !$wpsl_settings['hide_distance'] ) {
    //     $listing_template .= "\t\t" . '<%= distance %> ' . esc_html( $wpsl_settings['distance_unit'] ) . '' . "\r\n";
    // }

    $listing_template .= "\t\t" . '<%= createDirectionUrl() %>' . "\r\n";

    $listing_template .= "\t\t" . '</div>' . "\r\n";

    $listing_template .= "\t" . '</li>' . "\r\n";

    return $listing_template;
}

/* Load Custom WPSL Template
----------------------------*/
add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {

    /**
     * The 'id' is for internal use and must be unique ( since 2.0 ).
     * The 'name' is used in the template dropdown on the settings page.
     * The 'path' points to the location of the custom template,
     * in this case the folder of your active theme.
     */
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_template_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}

/* Remove autop from custom field
---------------------------------*/
function the_field_without_wpautop( $field_name ) {

	remove_filter('acf_the_content', 'wpautop');

	the_field( $field_name );

	add_filter('acf_the_content', 'wpautop');

}

/* Custom upload directory for pdf files
----------------------------------------*/
// add_filter('wp_handle_upload_prefilter', 'bs_pre_upload');
// add_filter('wp_handle_upload', 'bs_post_upload');

// function bs_pre_upload($file){
//     add_filter('upload_dir', 'bs_custom_upload_dir');
//     return $file;
// }

// function bs_post_upload($fileinfo){
//     remove_filter('upload_dir', 'bs_custom_upload_dir');
//     return $fileinfo;
// }

// function bs_custom_upload_dir($path){
//     $extension = substr(strrchr($_POST['name'],'.'),1);
//     if(!empty($path['error']) ||  $extension != 'pdf') { return $path; } //error or other filetype; do nothing.
//     $customdir = '/docs';
//     $path['path']    = str_replace($path['subdir'], '', $path['path']); //remove default subdir (year/month)
//     $path['url']     = str_replace($path['subdir'], '', $path['url']);
//     $path['subdir']  = $customdir;
//     $path['path']   .= $customdir;
//     $path['url']    .= $customdir;
//     return $path;
// }