
<?php if ( !is_user_logged_in() ) { ?>

<?php } else { ?>
	<nav id="site-navigation" class="main-navigation" role="navigation">
		<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
	</nav>
<?php } ?>


<p class="tml-rememberme-wrap">
	<input name="rememberme" type="checkbox" id="rememberme<?php $template->the_instance(); ?>" value="forever" />
	<label for="rememberme<?php $template->the_instance(); ?>"><?php esc_attr_e( 'Remember Me', 'theme-my-login' ); ?></label>
</p>


<header id="portal-mobile-masthead" class="mobile-header" role="banner">

	<div class="portal-mobile-bg">

		<a id="nav-trigger" href="#mobile-navigation"><i class="fa fa-bars"></i></a>
		<div id="portal-mobile-logo-wrap">
			<a class="home-link" href="<?php echo get_option('home'); ?>">
				<img class="portal-mobile-logo" src="<?php bloginfo('template_url'); ?>/img/mobile_wordmark.png">
			</a>
		</div>
	</div>

</header>


<h4 class="testimonial-type"><?php the_field('testimonial_type_2'); ?></h4>
<hr class="black-line">
<div class="container">
    <!-- check if the repeater field has rows of data -->
    <?php if( have_rows('testimonials_2') ): ?>

        <?php while ( have_rows('testimonials_2') ) : the_row(); ?>
			<div class="half">

                <span class="stars">
                    <?php
                        $select = get_sub_field_object('stars_2');
                        $value = get_sub_field('stars_2');
                    ?>
                    <?php foreach( $select['choices'] as $k => $v ): ?>
                        <?php if( $k == $value ): ?>
                            <?php
                            if ($value == 'four') {
                                echo '<i class="fa fa-star"></i>';
                                echo '<i class="fa fa-star"></i>';
                                echo '<i class="fa fa-star"></i>';
                                echo '<i class="fa fa-star"></i>';
                            }
                            else if ($value == 'five') {
                                echo '<i class="fa fa-star"></i>';
                                echo '<i class="fa fa-star"></i>';
                                echo '<i class="fa fa-star"></i>';
                                echo '<i class="fa fa-star"></i>';
                                echo '<i class="fa fa-star"></i>';
                            }
                            ?>

                        <?php endif; ?>

                    <?php endforeach; ?>

                </span>
                <p class="testimonial"> <?php the_sub_field('review_2'); ?> </p>
                <span class="source"> <?php  the_sub_field('source_2'); ?> </span>
			</div>
    <?php endwhile; ?>

    <?php endif; ?>

</div>

<a href="" class="hex-button">
    <img src="<?php bloginfo('template_url'); ?>/img/buttons/view-more.png">
</a>