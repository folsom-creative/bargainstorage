<?php
/**
 * Template Name: Testimonials
 *
 * @package bargainstorage
 */
 if ( is_user_logged_in() ) {
 } else {
 	wp_redirect('http://bargainstorage.bldsvr.com/bargainstoragev2/login/'); exit;
 }
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="entry-content">
            <section class="grid-bg">
				<h1 class="section-heading"><?php the_field('main_heading'); ?></h1>
				<p class="section-tagline">
					<?php the_field_without_wpautop('top_content'); ?>
                </p>
                
                <!-- check if the repeater field has rows of data -->
                <?php if( have_rows('testimonials_1') ): ?>

                    <div class="container">
                    <?php while ( have_rows('testimonials_1') ) : the_row(); ?>
                        <?php if(get_sub_field('bold_headline')) { ?>
                            <h4 class="testimonial-type"><?php the_sub_field('bold_headline'); ?></h4>
                            <hr class="black-line">
                        <?php } else { ?>
                            
                        <?php } ?>
						<div class="half">

                            <span class="stars">
                                <?php
                                    $select = get_sub_field_object('stars_1');
                                    $value = get_sub_field('stars_1');
                                ?>
                                <?php foreach( $select['choices'] as $k => $v ): ?>
                                    <?php if( $k == $value ): ?>
                                        <?php
                                        if ($value == 'four') {
                                            echo '<i class="fa fa-star"></i>';
                                            echo '<i class="fa fa-star"></i>';
                                            echo '<i class="fa fa-star"></i>';
                                            echo '<i class="fa fa-star"></i>';
                                        }
                                        else if ($value == 'five') {
                                            echo '<i class="fa fa-star"></i>';
                                            echo '<i class="fa fa-star"></i>';
                                            echo '<i class="fa fa-star"></i>';
                                            echo '<i class="fa fa-star"></i>';
                                            echo '<i class="fa fa-star"></i>';
                                        }
                                        ?>

                                    <?php endif; ?>

                                <?php endforeach; ?>

                            </span>
                            <p class="testimonial"> <?php the_sub_field('review_1'); ?> </p>
                            <span class="source"> <?php  the_sub_field('source_1'); ?> </span>
						</div>
                    <?php endwhile; ?>

				    </div><!-- .container -->
                <?php endif; ?>

				

            </section>
        </div> <!-- .entry-content -->
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
