<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bargainstorage
 */

?>

<div class="wrapper">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">

			<?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>

			<?php if ( 'post' === get_post_type() ) : ?>
				<div class="entry-meta">
					<i class="fa fa-clock-o"></i>
					<span> <?php the_time('F jS, Y') ?> </span>
					<i class="fa fa-folder"></i>
					<span><?php the_tags(); ?></span>
					<i class="fa fa-user"></i>
					<span>by</span>
					 <span> <?php the_author(); ?> </span>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<?php
		// This snippet gets the url of the current page's featured image
		$imgURL = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		?>

		<?php if ( has_post_thumbnail() ) { ?>
			<div class="header-image" style="background-image: url(<?php echo $imgURL; ?>);">

			</div>

		<?php } else { ?>

		<?php } ?>

		<div class="content">
			<?php
				the_excerpt( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'bargainstorage' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			?>

			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bargainstorage' ),
					'after'  => '</div>',
				) );
			?>
		</div>

		<footer class="entry-footer">
			<!-- <?php bargainstorage_entry_footer(); ?> -->
		</footer><!-- .entry-footer -->
	</article><!-- #post-## -->
</div>  <!-- wrapper -->
