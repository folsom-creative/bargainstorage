<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bargainstorage
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">


		<div class="entry-meta">
			<i class="fa fa-clock-o"></i>
			<span> <?php the_time('F jS, Y') ?> </span>
			<i class="fa fa-folder"></i>
			<span><?php the_tags(); ?></span>
			<i class="fa fa-user"></i>
			<span>by</span>
			 <span> <?php the_author(); ?> </span>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div>
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bargainstorage' ),
				'after'  => '</div>',
			) );
		?>
	</div>

	<footer class="entry-footer">
		<?php bargainstorage_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
