<?php
/**
 * Template Name: Press Releases
 *
 * @package bargainstorage
 */
?>

    <h1 class="section-heading"><?php the_title(); ?></h1>

    <div id="primary" class="content-area two-thirds">
    	<main id="main" class="site-main" role="main">

			<div class="entry-content">
	            <section>
                    <h3 class="headline"><?php the_field('content_heading'); ?></h3>
                    <?php the_field('content'); ?>
	            </section>
	        </div> <!-- .entry-content -->

    </main><!-- #main -->
</div><!-- #primary -->

<div id="secondary" class="one-third" role="complementary">
    <section>
        <h5 class="list-title">PRESS RELEASE LIST</h5>
        <?php
            $loop = new WP_Query(
                array(
                'post_type' => 'press_release',
            )
        );
        ?>

        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                <a class="press-link" href="<?php echo get_permalink($post->ID);  ?> "> <span class=""><?php the_title(); ?></span> </a> <br>

        <?php endwhile; wp_reset_query(); // reset query at end of the loop. ?>
    </section>
</div>
