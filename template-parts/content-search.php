<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bargainstorage
 */

?>

<?php if ( 'post' == get_post_type() ) : ?>	<!--blog post-->

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

			<?php if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<i class="fa fa-clock-o"></i>
				<span> <?php the_time('F jS, Y') ?> </span>
				<i class="fa fa-folder"></i>
				<span><?php the_tags(); ?></span>
				<i class="fa fa-user"></i>
				<span>by</span>
				 <span> <?php the_author(); ?> </span>
			</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->

		<footer class="entry-footer">
			<?php bargainstorage_entry_footer(); ?>
		</footer><!-- .entry-footer -->
	</article><!-- #post-## -->

<?php endif; ?>
