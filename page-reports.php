<?php
/**
 * Template Name: Reports
 *
 * @package bargainstorage
 */
 if ( is_user_logged_in() ) {
 } else {
 	wp_redirect('http://bargainstorage.bldsvr.com/bargainstoragev2/login/'); exit;
 }
get_header(); ?>

<?php $user_ID = get_current_user_id(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="entry-content">
			<?php while ( have_posts() ) : the_post(); ?>
	            <section>
					<h1 class="section-heading"><?php the_field('reports_heading'); ?></h1>
					<div class="section-tagline"><?php the_content(); ?></div>

					<?php if( have_rows('reports', 'user_' . $user_ID) ): ?>

					    <ul class="report-downloads">

					    <?php while( have_rows('reports', 'user_' . $user_ID) ): the_row(); ?>

					        <li><a target="_blank" href="<?php the_sub_field('report_file'); ?>"><?php the_sub_field('report_title'); ?></a></li>

					    <?php endwhile; ?>

					    </ul>

					<?php endif; ?>

	            </section>
        	<?php endwhile; ?>
        </div> <!-- .entry-content -->
    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
