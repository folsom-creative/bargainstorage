<?php
/**
 * Template Name: Who We Are
 *
 * @package bargainstorage
 */

get_header(); ?>

<?php
// This snippet gets the url of the current page's featured image
$imgURL = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
?>
<div id="gif-hero">
    <img src="<?php echo $imgURL; ?>"/>
</div>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="entry-content">
            <section class="grid-bg">
                <div class="scallop-top"></div>
                <h1 class="section-heading"><?php the_field('main_heading'); ?></h1>
                <p class="section-tagline">
                    <?php the_field_without_wpautop('top_content'); ?>
                </p>
                <div class="scallop-bottom"></div>
            </section>
            <section class="grid-bg brown offset">
                <div class="holder">
                    <div class="quarter">
                        <i class="fa fa-heart-o"></i>
                        <h5><?php the_field('heart_heading'); ?></h5>
                        <p>
                            <?php the_field_without_wpautop('heart_text'); ?>
                        </p>
                    </div>
                    <div class="quarter">
                        <i class="fa fa-check-square-o"></i>
                        <h5><?php the_field('check_box_heading'); ?></h5>
                        <p>
                            <?php the_field_without_wpautop('check_box_text'); ?>
                        </p>
                    </div>
                    <div class="quarter">
                        <i class="fa fa-star-o"></i>
                        <h5><?php the_field('star_heading'); ?></h5>
                        <p>
                            <?php the_field_without_wpautop('star_text'); ?>
                        </p>
                    </div>
                    <div class="quarter">
                        <img class="faux-bulb" src="<?php bloginfo('template_url'); ?>/img/lightbulb.png"/>
                        <h5><?php the_field('lightbulb_heading'); ?></h5>
                        <p>
                            <?php the_field_without_wpautop('lightbulb_text'); ?>
                        </p>
                    </div>
                </div>
            </section>
            <section class="grid-bg">
                <div class="scallop-top"></div>
                <h1 class="section-heading"><?php the_field('section_heading'); ?></h1>
                <p class="section-tagline pad">
                    <?php the_field_without_wpautop('section_text'); ?>
                </p>
                <div class="holder">

                    <?php
					    $loop = new WP_Query(
						    array(
							'post_type' => 'teammate',
                            'category_name' => 'marketing'
						)
					);
				    ?>

                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

                        <?php
                            // get url of current post's featured image
                            $imgPath = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
                        ?>

                        <div class="third">
                            <div class="team-pic" class style="background-image: url('<?php echo $imgPath; ?>');">

                            </div>
                            <div class="team-pic-fix">

                                <span class="name-underline"> <?php the_title(); ?> </span>
                                <div class="employee-description">
                                    <span> <?php the_field('job_title'); ?>,</span> <br>
                                    <span> <?php the_field('location'); ?> </span>
                                </div>

                            </div>
                            <p class="employee-review">
                                <?php the_field('customer_quote'); ?>
                            </p>
                            <span class="employee-source"> <?php the_field('quote_source'); ?> </span>
                        </div>

                    <?php endwhile; wp_reset_query();// End of the loop. ?>

                </div>
                <div class="scallop-bottom"></div>
            </section>
            <div class="banner-image" style="background-image: url(<?php bloginfo('template_url'); ?>/img/who-we-are-footer.jpg);">
            </div>
        </div> <!-- .entry-content -->
    </main><!-- #main -->
</div><!-- #primary -->

<script type="text/javascript">
    jQuery(document).ready(function($){
        function imgHeight() {
            $('.team-pic').each(function(){
                picWidth = $(this).width();
                $(this).height(picWidth);
            });
        }

        $(window).resize(function() {
            imgHeight();
        }).resize();
    });
</script>
<?php get_footer(); ?>
