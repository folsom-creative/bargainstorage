<?php
/**
 * Template Name: Lost Password
 *
 * @package bargainstorage
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="entry-content">
				<section class="tml-wrap">
				    <div class="scallop-top"></div>
					<h1 class="section-heading">Reset Password</h1>
					<p class="section-tagline">Enter your username or email and we'll send you a reset email shortly.</p>
					<?php theme_my_login(array( 'default_action' => 'lostpassword', 'show_title' => 0 )); ?>
				</section>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
