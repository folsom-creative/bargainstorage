<?php
/**
 * Template Name: Storage Facilities
 *
 * @package bargainstorage
 */

get_header(); ?>

<div id="gif-hero">
    <img src="<?php bloginfo('template_url'); ?>/img/BS_Slim.gif"/>
</div>

<div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="entry-content">
                <section id="find-a-storage-unit" class="grid-bg unpad">
                    <div class="scallop-top"></div>
                    <h3><?php the_field('map_header'); ?></h3>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php the_content(); ?>
                    <?php endwhile; // End of the loop. ?>
                    <div class="scallop-bottom"></div>
                </section>

                <?php if( have_rows('reviews') ): ?>
                <section class="brown">
                    <img class="custom-prev" src="<?php bloginfo('template_url'); ?>/img/arrowLeft.png">
                    <img class="custom-next" src="<?php bloginfo('template_url'); ?>/img/arrowRight.png">

                    <div class="review-slider" style="min-height: 24vw;">
                        <div class="owl-carousel">

                            <?php while( have_rows('reviews') ): the_row(); ?>
                                <div>
                                    <p><?php the_sub_field('review'); ?></p>
                                    <span><?php the_sub_field('source'); ?></span>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </section>
                <?php endif; ?>

                <section class="grid-bg pad">
                    <div class="scallop-top"></div>

                    <h1 class="section-heading"><?php the_field('gallery_header'); ?></h1>

                    <div id="filters" class="filter">
                        <div class="menu">
                            <!-- <button data-filter="*" id="blogs" class="button">See All</button> -->
                            <button data-filter=".quality" id="quality" class="button">Quality Storage Spaces</button>
                            <button data-filter=".access" id="access" class="button">Easy Access</button>
                            <button data-filter=".security" id="security" class="button">High Level Security</button>
                            <button data-filter=".service" id="service" class="button">Honest Friendly Service</button>
                            <button data-filter=".parking" id="parking" class="button">Convenient RV Parking</button>
                            <button data-filter=".cooled" id="cooled" class="button">Cooled Storage</button>
                        </div>
                    </div>

                    <?php 
                        if( have_rows('facilities_gallery') ):
                            echo '<div id="gallery">';
                            echo '<div class="gutter-sizer"></div>';
                            echo '<div class="grid-sizer"></div>';
                         
                            while ( have_rows('facilities_gallery') ) : the_row();
                                $field_obj = get_sub_field_object( 'image_tag' );
                                $tags = get_sub_field('image_tag'); 
                    ?>
                                <a class="gallery-item <?php echo join(' ', $tags); ?>" style="background-image: url('<?php the_sub_field('image_file'); ?>')" href="<?php the_sub_field('image_file'); ?>">
                                    <div class="tag-bg">
                                        <span class="img-tag">
                                        <?php foreach ( $tags as $tag ){
                                            echo $field_obj['choices'][ $tag ] . '<span class="comma">, </span>';
                                        } ?>
                                        </span>
                                    </div>
                                </a>
                    <?php 
                            endwhile;
                            echo '</div>';
                        else :

                            // no rows found

                        endif;
                    ?>

                </section>
            </div> <!-- .entry-content -->

        </main><!-- #main -->
</div><!-- #primary -->

<script type="text/javascript">
    jQuery(document).ready(function($){

        /* Owl Carousel (Reviews slider)
        --------------------------------*/
        var owl = $('.owl-carousel');

        owl.owlCarousel({
            loop:true,
            margin:10,
            nav:false,
            items: 1,
            center: true,
            draggable: true
        });

        // Go to the next item
        $('.custom-next').click(function() {
            owl.trigger('next.owl.carousel');
        })

        // Go to the previous item
        $('.custom-prev').click(function() {
            // With optional speed parameter
            // Parameters have to be in square brackets '[]'
            owl.trigger('prev.owl.carousel', [300]);
        })


        /* Isotope Gallery
        ------------------*/
        var $isoContainer = $('#gallery');
        var filterValue = '*';

        $isoContainer.imagesLoaded(function(){
            $isoContainer.isotope({
                filter: filterValue,
                itemSelector: '.gallery-item'
            });

        /* Magnific Lightbox Gallery
        ----------------------------*/
            $('.gallery-item').magnificPopup({
                type: 'image',
                gallery:{
                    enabled: true
                }
            });
        });

        // apply filters
        $('#filters').on( 'click', 'button', function() {

            filterValue = $(this).attr('data-filter');

            $isoContainer.isotope({
                filter: filterValue,
                itemSelector: '.gallery-item'
            });
        });


        $('.button').click(function(){
            $('.button').removeClass('active');
            $(this).addClass('active');

        });
    });
</script>

<?php get_footer(); ?>
