<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bargainstorage
 */

// if (is_user_logged_in() && is_front_page() ) {
// 	wp_redirect('/dashboard');
// 	exit;
// }

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content" tabindex="20"><?php esc_html_e( 'Skip to content', 'bargainstorage' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<?php if (is_page('dashboard') || is_page('reports')): ?>
			<div class="portal-bg">

				<div class="portal-nav-wrapper">
					<nav id="mini-navigation" class="main-navigation" role="navigation">
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
					</nav>
					<span class="user-name">
						<?php global $current_user;
						get_currentuserinfo();
	      				echo 'Welcome, ' . $current_user->user_login . "!"; ?>
					 </span>
					<a class="logout-link" href="<?php echo wp_logout_url(); ?>"><img class="logout-button" src="<?php bloginfo('template_url'); ?>/img/buttons/logout.png"/></a>
				<!-- <div class="portal-social-wrapper">
						<a href="https://www.facebook.com/Glendale-Bargain-Storage-297689910318476/timeline/" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="https://twitter.com/bargainstorage" target="_blank"><i class="fa fa-twitter"></i></a>
						<a href="https://www.youtube.com/channel/UCKHnyMpBaZXClVUtRNdYxSA" target="_blank"><i class="fa fa-youtube"></i></a>
						<a href="https://instagram.com/bargainstorage/" target="_blank"><i class="fa fa-instagram"></i></a>
					</div> -->
				</div>

				<div class="portal-logo-wrapper">
					<a class="logo" href="<?php echo get_option('home'); ?>">
						<img src="<?php bloginfo('template_url'); ?>/img/footerLogo.png">
					</a>
				</div>

				<nav id="portal-navigation" class="portal-navigation" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'portal', 'menu_id' => 'portal-menu' ) ); ?>
				</nav>

			</div>
		<?php else: ?>
			<div class="top-wrapper">
				<div class="left">
					<div class="content">
						<?php if(is_page('home') || is_page('storage-facilities')) { ?>
							<a href="#find-a-storage-unit" class="scrollto">
								<img src="<?php bloginfo('template_url'); ?>/img/location.png">
								<h5>FIND LOCATION</h5>
							</a>
						<?php } else { ?>
							<a href="<?php echo get_permalink(73); ?>">
								<img src="<?php bloginfo('template_url'); ?>/img/location.png">
								<h5>FIND LOCATION</h5>
							</a>
						<?php } ?>
					</div>
				</div>

				<div class="middle vertical-parent">
					<div class="vertical-center">
						<a class="logo" href="<?php echo get_option('home'); ?>">
							<img src="<?php bloginfo('template_url'); ?>/img/logo.png">
						</a>
					</div>
				</div>

				<div class="right">
					<div class="content">
					<a href="/dashboard">
					<img src="<?php bloginfo('template_url'); ?>/img/ReportIcon.png" style="height: 75px; padding-top: 8px; display: block; margin: 0 auto; margin-bottom: 16px;">
					<h5>Reports</h5>
					</a>
						<!-- <div class="social">
							<a href="https://www.facebook.com/Glendale-Bargain-Storage-297689910318476/timeline/" target="_blank"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/bargainstorage" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.youtube.com/channel/UCKHnyMpBaZXClVUtRNdYxSA" target="_blank"><i class="fa fa-youtube"></i></a>
							<a href="https://instagram.com/bargainstorage/" target="_blank"><i class="fa fa-instagram"></i></a>
						</div> -->
						<!-- <a id="login-link" href="<?php echo get_permalink(523); ?>"><h5>LOGIN REGISTER</h5></a> -->
					</div>
				</div>
			</div>
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav>
		<?php endif; ?>
	</header><!-- #masthead -->

	<?php if (is_page('dashboard') || is_page('reports')): ?>
		<header id="portal-mobile-masthead" class="mobile-header" role="banner">

			<div>

				<a id="nav-trigger" href="#mobile-navigation"><i class="fa fa-bars"></i></a>
				<div id="portal-mobile-logo-wrap">
					<a class="home-link" href="<?php echo get_option('home'); ?>">
						<img class="portal-mobile-logo" src="<?php bloginfo('template_url'); ?>/img/mobile-logo-white.png">
					</a>
				</div>
			</div>

		</header>
		<?php else: ?>
		<header id="mobile-masthead" class="mobile-header" role="banner">
			<div class="mobile-blue-bar">

				<a id="nav-trigger" href="#mobile-navigation"><i class="fa fa-bars"></i></a>
				<div id="mobile-logo-wrap">
					<a class="home-link" href="<?php echo get_option('home'); ?>">
						<div class="band-aid"></div>
						<img class="m-logo" src="<?php bloginfo('template_url'); ?>/img/mobile_mark.png">
					</a>
				</div>
			</div>
		</header>
		<div class="mobile-word-wrap">
			<img class="word-logo" src="<?php bloginfo('template_url'); ?>/img/mobile_wordmark.png"/>
		</div>
	<?php endif; ?>

	<div id="content" class="site-content">
