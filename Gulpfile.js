var gulp = require('gulp'),
    sass = require('gulp-sass')
    //neat = require('node-neat').includePaths;

var paths = {
    scss: 'style.scss'
};

gulp.task('styles', function () {
    return gulp.src(paths.scss)
        .pipe(sass({
            includePaths: ['styles'].concat()
        }))
        .pipe(gulp.dest(''));
});

gulp.task('default',function(){
    gulp.start('styles');
});