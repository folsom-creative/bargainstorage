<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bargainstorage
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<a href="<?php echo get_option('home'); ?>">
			<img class="footer-logo" src="<?php bloginfo('template_url'); ?>/img/footerLogo.png">
		</a>
		<h3 class="address"><span>We&nbsp;give&nbsp;you a&nbsp;great&nbsp;deal</span><span> &middot; </span><span>We&nbsp;don’t&nbsp;cut&nbsp;corners</span></h3>
        <div class="social">
            <a href="https://www.facebook.com/Glendale-Bargain-Storage-297689910318476/timeline/" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="https://twitter.com/bargainstorage" target="_blank"><i class="fa fa-twitter"></i></a>
            <a href="https://www.youtube.com/channel/UCKHnyMpBaZXClVUtRNdYxSA" target="_blank"><i class="fa fa-youtube"></i></a>
            <a href="https://instagram.com/bargainstorage/" target="_blank"><i class="fa fa-instagram"></i></a>
        </div>
		<h4 class="copyright">© COPYRIGHT &nbsp;&nbsp;2015 BARGAIN STORAGE</h4>
	</footer><!-- #colophon -->

    <?php if(is_page_template( 'page-home.php' )){ ?>
        <footer id="sticky-footer">
            <div class="link-wrap">
                <a href="<?php echo get_permalink(73); ?>" id="locations" class="sticky-link">Locations</a>
            </div>
            <div id="gif-wrap">
                <a href="<?php echo get_option('home'); ?>">
                    <img src="<?php bloginfo('template_url'); ?>/img/BS_Mobile.gif">
                </a>
            </div>
            <div class="link-wrap right">
                <a href="<?php echo get_permalink(523); ?>" id="login" class="sticky-link">Login</a>
            </div>
        </footer>
    <?php }; ?>

</div><!-- #page -->

<?php if(is_user_logged_in()) { ?>
    <nav id="mobile-navigation" class="main-navigation" role="navigation">
        <?php
        $defaults = array(
            'theme_location'  => 'portal-mobile',
            'menu'            => '',
            'container'       => false,
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => '',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        );
        wp_nav_menu( $defaults );
        ?>
    </nav>
<?php } else { ?>
    <nav id="mobile-navigation" class="main-navigation" role="navigation">
        <?php
        $defaults = array(
            'theme_location'  => 'mobile',
            'menu'            => '',
            'container'       => false,
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => '',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        );
        wp_nav_menu( $defaults );
        ?>
    </nav>
<?php } ?>

<script type="text/javascript">
    jQuery(document).ready(function($){
        var winW = $(window).width();


        $('#wpsl-search-input').attr('placeholder', 'Enter ZIP');

        $('#wpsl-search-btn').click(function(){
            console.log(winW);
            if(winW < 768){
                $('#wpsl-result-list').fadeIn(1300);
            }
        });

        $('#map-toggle').click(function(){
            $('#wpsl-gmap').fadeToggle(1300);
        });

        $(window).resize(function(){
            winW = $(window).width();
        })
    });
</script>

<style type="text/css">
    #wpsl-gmap {
        height: 38vw !important;
    }
    @media screen and (max-width: 768px) {
        #wpsl-gmap {
            height: 60vw !important;
        }
    }
    @media only screen and (max-device-width: 800px) and (orientation: landscape) {
        #wpsl-gmap {
            display: none;
        }
    }
</style>
<?php wp_footer(); ?>

</body>
</html>
