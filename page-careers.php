<?php
/**
 * Template Name: Careers
 *
 * @package bargainstorage
 */

get_header(); ?>

<?php
// This snippet gets the url of the current page's featured image
$imgURL = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
?>
<div class="banner-image" style="background-image: url(<?php echo $imgURL; ?>);">

</div>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="entry-content">

            <section class="grid-bg">
                <div class="scallop-top"></div>
                <h1 class="section-heading"><?php the_field('main_heading'); ?></h1>
                <p class="section-tagline">
                    <?php the_field_without_wpautop('top_content'); ?>
                </p>
                <div>
                    <a id="learn-more" href="#jobs" class="hex-button scrollto">
                        <img src="<?php bloginfo('template_url'); ?>/img/buttons/learn-more.png">
                    </a>
                </div>
                <div class="holder">
                    <div class="quarter">
                        <i class="fa fa-heart-o"></i>
                        <h5><?php the_field('heart_heading'); ?></h5>
                        <p>
                            <?php the_field_without_wpautop('heart_text'); ?>
                        </p>
                    </div>
                    <div class="quarter">
                        <i class="fa fa-check-square-o"></i>
                        <h5><?php the_field('check_box_heading'); ?></h5>
                        <p>
                            <?php the_field_without_wpautop('check_box_text'); ?>
                        </p>
                    </div>
                    <div class="quarter">
                        <i class="fa fa-star-o"></i>
                        <h5><?php the_field('star_heading'); ?></h5>
                        <p>
                            <?php the_field_without_wpautop('star_text'); ?>
                        </p>
                    </div>
                    <div class="quarter">
                        <img class="faux-bulb" src="<?php bloginfo('template_url'); ?>/img/lightbulb.png"/>
                        <h5><?php the_field('lightbulb_heading'); ?></h5>
                        <p>
                            <?php the_field_without_wpautop('lightbulb_text'); ?>
                        </p>
                    </div>
                </div>
                <div class="scallop-bottom"></div>
            </section> <!-- .wrapper -->

            <?php if( have_rows('reviews') ): ?>

                <section class="brown">
                    <img class="custom-prev" src="<?php bloginfo('template_url'); ?>/img/arrowLeft.png">
                    <img class="custom-next" src="<?php bloginfo('template_url'); ?>/img/arrowRight.png">

                    <div class="review-slider" style="min-height: 24vw;">
                        <div class="owl-carousel">

                            <?php while( have_rows('reviews') ): the_row(); ?>

                                <div>
                                    <p><?php the_sub_field('review'); ?></p>
                                    <span><?php the_sub_field('source'); ?></span>
                                </div>

                            <?php endwhile; ?>
                        </div>
                    </div>
                </section>

            <?php endif; ?>

            <?php if( have_rows('open_positions') ): ?>

                <section id="jobs" class="grid-bg">
                    <div class="scallop-top"></div>

                    <?php while( have_rows('open_positions') ): the_row(); ?>

                        <div class="half job">
                            <div class="drawing">
                                <img src="<?php the_sub_field('job_image'); ?>">
                            </div>
                            <h5 class="title"><?php the_sub_field('job_title'); ?></h5>
                            <?php the_sub_field('job_description'); ?>
                        </div>

                    <?php endwhile; ?>

                </section> <!-- .wrapper -->

            <?php endif; ?>

            <section class="grid-bg pad">

                <div class="application-form">
                    <?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
                </div>

            </section> <!-- .wrapper -->
        </div> <!-- .entry-content -->
    </main><!-- #main -->
</div><!-- #primary -->

<script type="text/javascript">
    jQuery(document).ready(function($){
        var owl = $('.owl-carousel');

        owl.owlCarousel({
            autoplay:true,
            autoplayTimeout:3500,
            autoplayHoverPause:true,
            loop:true,
            margin:10,
            nav:false,
            items: 1,
            center: true,
            draggable: true
        });

        // Go to the next item
        $('.custom-next').click(function() {
            owl.trigger('next.owl.carousel');
        })

        // Go to the previous item
        $('.custom-prev').click(function() {
            // With optional speed parameter
            // Parameters have to be in square brackets '[]'
            owl.trigger('prev.owl.carousel', [300]);
        })

        $("#learn-more").click(function() {
            $('html, body').animate({
                scrollTop: $("#jobs").offset().top
            }, 1500);
        });
    });
</script>
<?php get_footer(); ?>
