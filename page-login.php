<?php
/**
 * Template Name: Login/Register
 *
 * @package bargainstorage
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="entry-content">
				<?php if(!is_user_logged_in()) { ?>
					<section class="tml-wrap">
					    <h1 class="section-heading">Login</h1>
					    <p class="section-tagline">To view your reports and to learn about what we’ve been up to recently, enter your information below.</p>
						<?php theme_my_login(array( 'default_action' => 'login', 'show_title' => 0 )); ?>
					</section>
				<?php } ?>

				<section class="grid-bg tml-wrap">
				    <div class="scallop-top"></div>
					<h1 class="section-heading">Request Access</h1>
					<p class="section-tagline">To learn more about our proven formula and brand promise, please provide us with the following information.</p>
					<?php theme_my_login(array( 'default_action' => 'register', 'show_title' => 0 )); ?>
				</section>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
