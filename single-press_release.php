<?php
/**
 * The template for displaying all single press posts
 *
 * @package bargainstorage
 */
 if ( is_user_logged_in() ) {
 } else {
    wp_redirect('http://bargainstorage.bldsvr.com/bargainstorage/login/'); exit;
 }
get_header(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'single-press_release' ); ?>

                    <?php the_post_navigation(); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template.
						// if ( comments_open() || get_comments_number() ) :
						// 	comments_template();
						// endif;
					?>

				<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
